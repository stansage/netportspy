/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package netportspy;
        
/**
 *
 * @author stansage
 */

import java.util.Iterator;
import java.util.List;

import org.pcap4j.core.*;
import org.pcap4j.core.BpfProgram.BpfCompileMode;
import org.pcap4j.core.PcapNetworkInterface.PromiscuousMode;
import org.pcap4j.packet.Packet;
import com.sun.jna.Platform;

@SuppressWarnings("javadoc")
public class NetPortSpy {

    private static final String READ_TIMEOUT_KEY    = NetPortSpy.class.getName() + ".readTimeout";
    private static final String SNAPLEN_KEY         = NetPortSpy.class.getName() + ".snaplen";
    private static final int READ_TIMEOUT           = Integer.getInteger(READ_TIMEOUT_KEY, 10); // [ms]
    private static final int SNAPLEN                = Integer.getInteger(SNAPLEN_KEY, 65536); // [bytes]

    private NetPortSpy() {}

    public static void main(String[] args) throws PcapNativeException, NotOpenException {
        if (args.length < 2) {
            System.err.println("usage: NetPortSpy interface port [packet_count]");
            return;
        }

        String iface = args[0];
        int port = 0;
        int packetCount = -1;
        PcapNetworkInterface nif = null;

        /// Parse command line arguments
        try {
            port = Integer.parseInt(args[1]);
            if (args.length > 2) {
                packetCount = Integer.parseInt(args[3]);
                if (packetCount  <= 0) {
                    packetCount = -1;
                }
            }
        } catch (NumberFormatException e) {
            System.err.println("invalid arguments passed, using defaults...");
        }

        /// Find specified network interface, or exit on error
        try {
            List nifList = Pcaps.findAllDevs();
            for (Iterator it = nifList.iterator(); it.hasNext();) {
                nif = (PcapNetworkInterface) it.next();
                if (nif.getName().trim().equals(iface)) {
                    break;
                }
                nif = null;
            }
        } catch (Exception e) {
          e.printStackTrace();
          return;
        }

        /// Print available network interfaces and exit
        if (nif == null) {
            System.err.println("The interface '" + args[0] + "' not present in system. There are current interfaces:");
            try {
                List nifList = Pcaps.findAllDevs();
                for (Iterator it = nifList.iterator(); it.hasNext();) {
                    nif = (PcapNetworkInterface) it.next();
                    System.err.print(nif.getName().trim());
                    System.err.print("(");
                    System.err.print(nif.getDescription().trim());
                    System.err.print(")");
                    if (it.hasNext()) {
                        System.err.print(", ");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return;
        }

        System.out.println("Spying interface + "+ iface + " and port " + String.valueOf(port));

        final PcapHandle handle = nif.openLive(SNAPLEN, PromiscuousMode.PROMISCUOUS, READ_TIMEOUT);
        final PacketListener listener = new PacketListener() {
            @Override
            public void gotPacket(Packet packet) {
                System.out.println(handle.getTimestamp());
                System.out.println(packet);
            }
        };

        /// Install the packet filter for destination port
        if (port > 0) {
            handle.setFilter("dst port " + String.valueOf(port), BpfCompileMode.OPTIMIZE);
        }

        /// Starts the capture loop
        try {
            handle.loop(packetCount, listener);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        /// Print packet total statistics
        PcapStat ps = handle.getStats();

        System.out.println("ps_recv: " + ps.getNumPacketsReceived());
        System.out.println("ps_drop: " + ps.getNumPacketsDropped());
        System.out.println("ps_ifdrop: " + ps.getNumPacketsDroppedByIf());

        if (Platform.isWindows()) {
            System.out.println("bs_capt: " + ps.getNumPacketsCaptured());
        }

        handle.close();
    }
}
