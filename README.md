**How to build**

* Download and install WinPcap from [here](https://www.winpcap.org/install/default.htm)
* Download and install Java Development Kit from [here](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* Download and install Apache Maven from [here](https://maven.apache.org/download.cgi)
* Clone sources from git [repository](https://bitbucket.org/stansage/netportspy.git)
* Build package using command `mvn package`

